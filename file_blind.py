#!/bin/python
# Copyright 2016 Jude Hungerford
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at 
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License. 

import csv
import uuid
import os

BLIND_STRING = "blind"
KEY_FILENAME = "key.csv"
PROTECTED_TAILS = [
    "ADME",
    "NSE"
]

class Blind:

    def __init__(self):
        """ Initialize """

    def get_filenames(self):
        """ get the filenames to obfuscate """
        l = os.listdir(".")
        return [i for i in l if not (i.endswith(".py") or (i == KEY_FILENAME) or i.startswith(BLIND_STRING) or os.path.isdir(i) or i.startswith("."))]

    def get_blind_name(self, prevblind, ext):
        found = False
        blind = ""
        while not found:
            blind = BLIND_STRING + "_" + str(uuid.uuid4().hex) + ext
            if blind not in prevblind:
                found = True
        return blind

    def protected_tail(self, filename):
        """ If the tail of the filename is protected, return it. Otherwise, return an empty string."""
        protected = ""
        prefix, _ = os.path.splitext(filename)
        for tail in PROTECTED_TAILS:
            if prefix.endswith(tail):
                protected = tail
        return protected

    def blind_filenames(self):
        fnames = self.get_filenames()
        blind_names = []
        for fname in fnames:
            _, ext = os.path.splitext(fname)
            extra_ext = self.protected_tail(fname)
            blind_names.append(self.get_blind_name(blind_names, extra_ext + ext))
        data = self.swap_axes([fnames, blind_names])
        self.write_csv(KEY_FILENAME, data)
        self.rename_files(data)

    def swap_axes(self, data):
        swap = []
#        colnum = -1
        for col in data:
#            colnum += 1
            rownum = -1
            for item in col:
                rownum += 1
                if rownum > (len(swap) - 1):
                    swap.append([])
                swap[rownum].append(item)
        return swap

    def write_csv(self, filename, data):
        outfile = open (filename, "w", encoding='latin1')
        writer = csv.writer (outfile, delimiter = ',')
        for line in data:
            writer.writerow (line)
        
    def rename_files(self, data):
        for row in data:
            pre = row[0]
            post = row[1]
            os.rename(pre, post)

    def blind_done(self):
        l = os.listdir(".")
        bdone = False
        for f in l:
            if f.startswith(BLIND_STRING):
                bdone = True
        return bdone

    def reverse_blind_filenames(self):
        #print("reverse blind not implemented")
        reverse_data = []
        with open (KEY_FILENAME, "r", encoding='latin1') as csvfile:
            reader = csv.reader(csvfile, delimiter=",")
            for line in reader:
                if len(line) > 1:
                    reverse_data.append([line[1], line[0]])
        self.rename_files(reverse_data)

    def run(self):
        if self.blind_done():
            self.reverse_blind_filenames();
        else:
            self.blind_filenames();

b = Blind()
b.run()
